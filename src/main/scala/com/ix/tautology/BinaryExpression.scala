package com.ix.tautology

import java.util

/**
  * Created by venki on 4/6/16.
  */
abstract class BinaryExpression {

  def eval(implicit config: Set[Char]): Boolean = this match {
    case And(al, ar) => al.eval && ar.eval
    case Or(ol, or) => ol.eval || or.eval
    case Not(nchild) => !nchild.eval
    case Const(bvalue) => config(bvalue)
  }

  override def toString: String = this match {
    case And(al, ar) => "(" + al.toString + "&" + ar.toString + ")"
    case Or(ol, or) => "(" + ol.toString + "|" + or.toString + ")"
    case Not(nchild) => "!" + nchild.toString
    case Const(bvalue) => bvalue.toString
  }

  def isTautology: Boolean = getVariablePermutations.forall {
    { implicit config =>
      this.eval
    }
  }

  def getVariables: Set[Char] = {
    VariablesParser.getVariables(this.toString)
  }

  def getVariablePermutations:Seq[Set[Char]] = {
    val variables = getVariables
    val nVars = variables.size
    for {
      i <- 0 until Math.pow(2,nVars).toInt
      bval = i.toBinaryString
      permutation = "0"*(nVars-bval.length) + bval
      varValues:Set[(Char,Char)] = variables zip permutation.toCharArray
      activeVars = varValues.map(x => if (x._2 == '1') x._1 else '0')
    } yield {
      activeVars
    }
  }

}

case class And(left: BinaryExpression, right: BinaryExpression) extends BinaryExpression

case class Or(left: BinaryExpression, right: BinaryExpression) extends BinaryExpression

case class Not(child: BinaryExpression) extends BinaryExpression

case class Const(value: Char) extends BinaryExpression





