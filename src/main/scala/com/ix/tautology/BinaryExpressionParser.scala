package com.ix.tautology

import scala.util.parsing.combinator.RegexParsers

/**
  * Created by venki on 4/6/16.
  */
class BinaryExpressionParser extends RegexParsers {

  def operator = """[&|]""".r

  def not = """!""".r

  def variable: Parser[BinaryExpression] = """[a-z]""".r ^^ { x => Const(x.toList.head) }

  def element: Parser[BinaryExpression] = variable | group

  def negatedElement: Parser[BinaryExpression] = not ~> element ^^ {
    case (elem) => Not(elem)
  }

  def exprNode: Parser[BinaryExpression] = negatedElement | element


  def group: Parser[BinaryExpression] = "(" ~> exprNode ~ operator ~ exprNode <~ ")" ^^ {
    case (a ~ "&" ~ c) => And(a, c)
    case (a ~ "|" ~ c) => Or(a, c)
  }

  def parseExpression(expr: String): Option[BinaryExpression] = {
    parseAll(group, expr) match {
      case Success(matched, remain) => Option(matched)
      case Failure(msg, _) =>
        println("Failure " + msg)
        None
      case Error(msg, _) =>
        println("Error " + msg)
        None
    }
  }
}


object BinaryExpressionParser {
  val parser: BinaryExpressionParser = new BinaryExpressionParser()
  def parseExpression(expr:String) ={
    parser.parseExpression(expr)
  }
}

