package com.ix.tautology

import scala.util.parsing.combinator.RegexParsers

/**
  * Created by venki on 4/8/16.
  */
class VariablesParser extends RegexParsers {
  def variable: Parser[String] =
    """[a-z]""".r ^^ {
      _.toString
    }

  def other: Parser[String] =
    """[^a-z]*""".r ^^ {
      _.toString
    }

  def vars: Parser[String] = other ~> variable <~ other

  def chunk:Parser[Set[String]] = rep1(vars) ^^ { x => x.toSet}

  def getVariables(expr:String):Set[Char] ={
    parseAll(chunk,expr) match {
      case Success(matched, remain) => matched.map(x=>x.charAt(0))
      case Failure(msg, _) =>
        println("Failure " + msg)
        Set.empty
      case Error(msg, _) =>
        println("Error " + msg)
        Set.empty
    }
  }

}

object VariablesParser {
  val parser = new VariablesParser()
  def getVariables(expr:String) = {
    parser.getVariables(expr)
  }
}
