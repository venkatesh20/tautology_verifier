package com.ix.tautology

import scala.util.parsing.combinator._

/**
  * Created by venki on 4/5/16.
  */

case class WordFreq(word: String, freq: Int) {
  override def toString = "Word <" + word + "> " + "occurs with frequency " + freq
}

class SimpleParser extends RegexParsers {
  def word: Parser[String] =
    """[a-z]""".r ^^ {
      _.toString
    }

  def number: Parser[Int] =
    """[0-9]+""".r ^^ {
      _.toInt
    }

  def wordFreq: Parser[WordFreq] = word ~ number ^^ {
    case wd ~ fr => WordFreq(wd, fr)
  }

  def variable: Parser[String] =
    """[a-z]""".r ^^ {
      _.toString
    }

  def other: Parser[String] =
    """[^a-z]*""".r ^^ {
      _.toString
    }

  def vars: Parser[String] = other ~> variable <~ other

  def chunk = rep1(vars) ^^ { x => x.map((_, 1)).toMap }

}

object TestSimpleParser extends SimpleParser {
  def main(args: Array[String]) = {
    parse(chunk, "((a & (!b | b)) | (!a & (!b | b)))") match {
      case Success(matched, remain) => println(matched)
      case Failure(msg, _) => println("Failure " + msg)
      case Error(msg, _) => println("Failure " + msg)
    }
  }
}
