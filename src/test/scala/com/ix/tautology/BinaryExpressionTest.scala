package com.ix.tautology

import org.scalatest.{FlatSpec, Matchers}

import scala.util.control.Exception

/**
  * Created by venki on 4/8/16.
  */
class BinaryExpressionTest extends FlatSpec with Matchers {


  behavior of "BinaryExpression"

  "(true & false)" should " = false " in {
    val bexp = And(Const('a'), Const('b'))
    implicit val config = Set('a')
    assert(!bexp.eval)
    assert {
      bexp.toString.equals("(a&b)")
    }
  }

  "(true | false)" should " = true " in {
    val bexp = Or(Const('a'), Const('b'))
    implicit val config = Set('a')
    assert(bexp.eval)
    assert {
      bexp.toString.equals("(a|b)")
    }
  }

  "((true | false) & true)" should " = true " in {
    val bexp = And(Or(Const('a'), Const('b')), Const('c'))
    implicit val config = Set('a', 'c')
    assert(bexp.eval)
    assert {
      bexp.toString.equals("((a|b)&c)")
    }
  }

  "((true | false) & (true & true))" should " = true " in {
    val bexp = And(Or(Const('a'), Const('b')), And(Const('c'), Const('d')))
    implicit val config = Set('a', 'c', 'd')
    assert(bexp.eval)
    assert {
      bexp.toString.equals("((a|b)&(c&d))")
    }
  }

  "((true | false) & (false & true))" should " = false " in {
    val bexp = And(Or(Const('a'), Const('b')), And(Const('c'), Const('d')))
    implicit val config = Set('a', 'd')
    assert(!bexp.eval)
    assert {
      bexp.toString.equals("((a|b)&(c&d))")
    }
  }

  "(!(true | false) & !(false & true))" should " = false " in {
    val bexp = And(Not(Or(Const('a'), Const('b'))), Not(And(Const('c'), Const('d'))))
    implicit val config = Set('a', 'd')
    assert(!bexp.eval)
    assert {
      bexp.toString.equals("(!(a|b)&!(c&d))")
    }
  }

  "(a|b)" should "have 4 permutations" in {
    val bexp = BinaryExpressionParser.parseExpression("(a|b)").get
    assert(bexp.getVariablePermutations.size == 4)
  }

  "(a|(b&c))" should "have 4 permutations" in {
    val bexp = BinaryExpressionParser.parseExpression("(a|(b&c))").get
    assert(bexp.getVariablePermutations.size == 8)
  }

  val tautologyTests: List[(String, Boolean)] = List(
    ("(!a | (a & a)) ", true),
    ("(!a | (b & !a)) ", false),
    ("(!a | a) ", true),
    ("((a & (!b | b)) | (!a & (!b | b))) ", true),
    ("(a|(b|(c|(d|(e|(f|(g|!(h|i))))))))",false)
  )

  def verifyTautology(x: (String, Boolean)) = {
    val expr = x._1
    val expectedResult = x._2
    "isTautology("+expr+")" should "=" + expectedResult in {
      val result = BinaryExpressionParser.parseExpression(expr) match {
        case Some(bexp) => bexp.isTautology
        case _ => false
      }
      result should equal (expectedResult)
    }
  }

  tautologyTests.foreach(verifyTautology)

}
