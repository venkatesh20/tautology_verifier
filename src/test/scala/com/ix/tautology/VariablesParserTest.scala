package com.ix.tautology

import org.scalatest.{FlatSpec, Matchers}

/**
  * Created by venki on 4/8/16.
  */
class VariablesParserTest extends FlatSpec with Matchers {

  behavior of "VariablesParserTest"

  val testVars = Map[String, Set[String]](
    "(!a | (a & a))" -> Set("a"),
    "(!a | (b & !a)) " -> Set("a", "b"),
    "(!a | a)" -> Set("a"),
    "((a & (!b | b)) | (!a & (!b | b)))" -> Set("a", "b"),
    "(a|(b|(c|(d|(e|(f|(g|(h|i))))))))" -> Set("a", "b", "c", "d", "e", "f", "g", "h", "i")
  )

  def checkVariableParsing(entry: (String, Set[String])) {
    val expr = entry._1
    val expectedResult = entry._2
    "The variables in the expression "+expr should " be "+expectedResult.mkString(",")  in {
      VariablesParser.getVariables(expr) should equal(expectedResult)
    }
  }

  testVars.foreach(checkVariableParsing)
}
