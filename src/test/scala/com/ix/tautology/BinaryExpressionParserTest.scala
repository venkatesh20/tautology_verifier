package com.ix.tautology

import org.scalatest.{FlatSpec, Matchers}

/**
  * Created by venki on 4/8/16.
  */
class BinaryExpressionParserTest extends FlatSpec with Matchers {


  behavior of "BinaryExpressionParserTest"

  val expressions: List[String] = List("" +
    "(!a | (a & a))",
    "(!a | (b & !a)) ",
    "(!a | a)",
    "((a & (!b | b)) | (!a & (!b | b))) ",
    "(a|(b|(c|(d|(e|(f|(g|!(h|i))))))))",
    "(a|(b|c))"
  )

  def checkExpression(expr: String) = {
    it should "parse " + expr + " successfully" in {
      val exprWoSpace = expr.replaceAll("""\s+""", "")
      BinaryExpressionParser.parseExpression(expr).getOrElse("").toString should equal(exprWoSpace)
    }

  }

  expressions.foreach(checkExpression)

}
