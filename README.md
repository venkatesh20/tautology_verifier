# Tautology Verifier #

## Implementation Notes ##

**Language: Scala**

There are three main components, namely

1. BinaryExpression
    * Implementation of binary expression as a Tree

2. BinaryExpressionParser
    * Parser implemention based on Parser combinators of Scala API
    * Responsible for parsing binary (infix) expression string in to BinaryExpression object

3. VariablesParser
    * Parses the number of unique variables in an expression
    * Used to compute the number of total states/permutations required to verify the tautology of the expression


### Execution Flow ###

1. Parse the binary (infix) expression string in to a valid BinaryExpression object
2. Find the number of variables in the expression
3. Compute the number of total states/permutations required to verify the tautology
4. BinaryExpression takes a permutation (i.e which variables are true in a particular permutation) as config and checks if the expression evaluates to true for all possible permutations.

### Testing ###

All the components mentioned above are tested and the test cases based on ScalaTest can be found in the /src/test directory

### Assumptions ###
1.   All   statements   that   are   given   as   input   will   be   syntactically   valid.   (You   dont   need   to   handle  
invalid sentences). 
2.   All   propositional   variables   will   be   single   letter   alphabets   and   no   more   than   10   variables   will   be  
there in any statement. (ie., cardinality of the vocabulary <= 10). 
3. The propositional statement can be arbitrarily nested. 
4.   You   can   expected   sentences   to   be   well   bracketed   in   case   of   binary   operations.   for   ex:   a   |   b   will  
always   be   (a   |   b)   even   if   it   is   the   only   operation   in   the   statement.   (You   dont   need   to   worry   about  
associativity) 
5. Whitespace can appear anywhere in the statement. 
6. Only &, |, ! will be used as logical operators. 
7. ! operator will always associate with the immediate right proposition. 